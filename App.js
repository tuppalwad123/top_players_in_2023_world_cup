import React from "react";
import { StyleSheet, View, ImageBackground, ScrollView } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Navbar from "./src/Navbar";
import ListOfPlayers from "./src/Listofplayers";
import { SafeAreaView } from "react-native-safe-area-context";
import PlayerDetails from "./src/PlayerDetails";
const Stack = createStackNavigator();

function HomeScreen() {
  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground
        source={require("./assets/bg.jpg")}
        style={styles.backgroundImage}
      >
        <Navbar />
        <ScrollView 
        VerticalScroll={true}
        showsVerticalScrollIndicator={false}
        style={styles.scrollView}>
          <ListOfPlayers />
        </ScrollView>
      </ImageBackground>
    </SafeAreaView>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="PlayerDetails" component={PlayerDetails} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
    width: "100%",
    height: "100%",
  },
  scrollView: {
    marginHorizontal: 20,
  },

});
