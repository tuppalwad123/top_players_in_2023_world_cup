import React, { useEffect, useState } from 'react';
import { View, Text, Image, ScrollView, StyleSheet } from 'react-native';
import Detail from '../Details'; // Update the path as needed

const Table = ({ children }) => <View style={styles.table}>{children}</View>;
const Row = ({ children }) => <View style={styles.tableRow}>{children}</View>;

function PlayerDetail({ route }) {
  const [player, setPlayer] = useState({});
  const { id } = route.params;

  useEffect(() => {
    const playerData = Detail.find((p) => p.id === id);
    setPlayer(playerData);
  }, [id]);

  return (
    <View style={styles.container}>
      <View style={styles.userDetails}>
        <Image style={styles.image} source={{ uri: player.img }} />
        <View style={styles.userInfo}>
          <Text style={styles.fullName}>{player.full_name}</Text>
          <Text style={styles.role}>{player.role}</Text>
          <Text>Total Runs: {player.TotalRuns}</Text>
          <Text>High Score: {player.HighestScore}</Text>
          <Text>Centuries: {player.centuries}</Text>
          <Text>Half Centuries: {player.fifties}</Text>
        </View>
      </View>
      <ScrollView horizontal={true} style={styles.scoreTableContainer}>
          <View style={styles.scoreTable}>
            <View style={styles.tableRow}>
              <Text style={styles.tableHeader}>Match No.</Text>
              <Text style={styles.tableHeader}>Runs</Text>
              <Text style={styles.tableHeader}>Balls</Text>
              <Text style={styles.tableHeader}>Fours</Text>
              <Text style={styles.tableHeader}>Sixes</Text>
              <Text style={styles.tableHeader}>Opponent</Text>
            </View>
            {player.score?.map((match, index) => (
              <View style={styles.tableRow} key={index}>
                <Text style={styles.cell}>{index + 1}</Text>
                <Text style={styles.cell}>{match.run}</Text>
                <Text style={styles.cell}>{match.balls}</Text>
                <Text style={styles.cell}>{match.fours}</Text>
                <Text style={styles.cell}>{match.sixes}</Text>
                <Text style={styles.cell}>{match.Opponent}</Text>
              </View>
            ))}
          </View>
        </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  userDetails: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginRight: 10,
  },
  userInfo: {
    flex: 1,
  },
  fullName: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  role: {
    fontSize: 16,
    marginBottom: 5,
  },
  scoreTable: {
    flex: 1,
    flexDirection: 'column',
    borderWidth: 1,
    borderColor: '#ddd',
    borderRadius: 8,
    overflow: 'hidden',
    justifyContent: 'center',
    alignContent: 'center',
    marginLeft: 10,
    padding: 10,
  },
  scoreTableContainer: {
    flex: 1,
  },
  table: {
    
    flexDirection: 'column',
        
  },
  tableRow: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#ddd',
    paddingVertical: 8,
  },
  tableHeader: {
    fontWeight: 'bold',
    flex: 1,
    textAlign: 'center',
    
  },
  cell: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 4,
  },
  scoreTableContainer: {
    marginTop: 10,
  },
});

export default PlayerDetail;
