import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native'; // Import useNavigation hook
import { TouchableOpacity } from 'react-native-gesture-handler';
import Data from '../TopPlayes'; // Update the path as needed

function ListOfPlayers( ) {
  const navigation = useNavigation(); // Use the useNavigation hook to get the navigation object

  return (
    <View style={styles.container}>
      <Text style={styles.playerList}>List of Top Players in the World cup 2023</Text>
      <View style={styles.cardsContainer}>
        {Data.map((player) => (
          <View style={styles.card} key={player.id}>
            <View style={styles.cardHeader}>
              <Text style={styles.playerName}>{player.Name}</Text>
            </View>
            <View style={styles.cardBody}>
              <Image
                source={{ uri: player.img }} // Assuming player.img is a URL
                style={styles.playerImage}
              />
              <View style={styles.playerDetails}>
                <Text><Text style={styles.bold}>Runs:</Text> {player.Runs}</Text>
                <Text><Text style={styles.bold}>Highest Score:</Text> {player.HighestScore}*</Text>
              </View>
            </View>

          <TouchableOpacity onPress={() => navigation.navigate('PlayerDetails', { id: player.id })}>
            <Text style={ styles.viewDetails
            }
              
            >View Details</Text>
          </TouchableOpacity>
          </View>
        ))}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    
  },
  playerList: {
    fontSize: 15,
    fontWeight: 'bold',
    textAlign: 'center',
    marginVertical: 10,
    backgroundColor: '#f0f0f0',
    padding: 10,
    borderRadius: 8,
    fontFamily: 'sans-serif-medium',
  },
  cardsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  card: {
    width: '100%', // Adjust as needed
    borderWidth: 1,
    borderColor: '#ddd',
    borderRadius: 8,
    overflow: 'hidden',
    marginBottom: 16,
  },
  cardHeader: {
    padding: 8,
    backgroundColor: '#f0f0f0',
  },
  playerName: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  cardBody: {
    flexDirection: 'row',
    padding: 8,
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  playerImage: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginRight: 8,
  },
  playerDetails: {
    flex: 1,
  },
  bold: {
    fontWeight: 'bold',
  },
  cardFooter: {
    borderTopWidth: 1,
    borderTopColor: '#ddd',
    padding: 8,
    alignItems: 'center',
    backgroundColor: '#f0f0f0',
  },
  viewDetails: {
    color: 'blue',
    fontWeight: 'bold',
    padding: 8,
    textAlign: 'center',
    backgroundColor: '#f0f0f0',
  },

});

export default ListOfPlayers;
