import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

function Navbar() {
  return (
    <View style={styles.navbar}>
      <Text style={styles.title}>
        WCC 2023
      </Text>
     
    </View>
  );
}

const styles = StyleSheet.create({
  navbar: {
    backgroundColor: '#3498db', // You can set your preferred background color
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'left',
    alignItems: 'left',
    top : 10,
    
  },
  title: {
    color: '#fff', // You can set your preferred text color
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default Navbar;
